# Gestion de projet sur Notion 

# Last CHANGELOG

## 0.1 Release (08/12/2021) 

* **Milestone** [Header](https://framagit.org/Swillam/gestion-de-projet-sur-notion/-/milestones/1)

* Fix [#1](https://framagit.org/Swillam/gestion-de-projet-sur-notion/-/issues/1) Logo website
* Fix [#2](https://framagit.org/Swillam/gestion-de-projet-sur-notion/-/issues/2) add link
* Fix [#3](https://framagit.org/Swillam/gestion-de-projet-sur-notion/-/issues/3) add social link
* Fix [#4](https://framagit.org/Swillam/gestion-de-projet-sur-notion/-/issues/4) Header CSS
